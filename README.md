# Remote Authentication using the Portuguese Citizen Card #

This is an assignment from the course of Software Engineering from the Integrated Masters in Computer and Telematics Engineering (University of Aveiro). It consist on a client application that enables any external application to request authentication and to query the user's data using the Portuguese Citizen Card's library pteidlibj. This works enables any organization or institution to have a generic solution that assures secure remote authentication and authenticated personal information based on simple REST API calls, enabling a user to authenticate on a specific service without ever needing to store usernames and passwords, as all security issues are dealed with on the client-side.

### Keywords ###

* JavaEE
* JavaFX
* pteidlibj
* Jetty
* REST

### Owners ###

The entire solution was developped by Rui Espinha Ribeiro ([Espinha](https://bitbucket.org/Espinha)) and David Silva ([dmpasilva](https://bitbucket.org/dmpasilva)).
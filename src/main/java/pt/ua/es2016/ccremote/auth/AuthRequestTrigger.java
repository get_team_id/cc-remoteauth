package pt.ua.es2016.ccremote.auth;

/**
 * Created by davidsilva on 16/05/16.
 */
public interface AuthRequestTrigger {
    void addAuthRequestListener(AuthRequestListener listener);
    void removeAuthRequestListener(AuthRequestListener listener);
    void fireAuthRequest(AuthRequestEvent event);
}

package pt.ua.es2016.ccremote.auth;

import java.util.EventObject;

/**
 * Created by davidsilva on 16/05/16.
 */
public class AuthRequestEvent extends EventObject {
    /**
     * Constructs an AuthRequestEvent.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public AuthRequestEvent(AuthRequest source) {
        super(source);
    }
}

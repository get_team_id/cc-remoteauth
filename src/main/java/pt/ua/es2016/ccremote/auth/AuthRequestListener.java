package pt.ua.es2016.ccremote.auth;

import java.util.EventListener;

/**
 * An interface that contains the methods required for performing an Authentication Request.
 */
public interface AuthRequestListener extends EventListener {

    /**
     * Called every time an Auth Request Event occurs.
     * @param event The event that occurred.
     */
    void authRequestOccurred(AuthRequestEvent event);
}

package pt.ua.es2016.ccremote.auth;

import java.util.EventListener;

/**
 * Created by davidsilva on 16/05/16.
 */
public interface AuthReplyListener extends EventListener {
    void authReplyOccurred(AuthReplyEvent event);
}

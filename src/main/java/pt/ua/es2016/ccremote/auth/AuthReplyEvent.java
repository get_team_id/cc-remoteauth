package pt.ua.es2016.ccremote.auth;

import java.util.EventObject;

/**
 * Created by davidsilva on 16/05/16.
 */
public class AuthReplyEvent extends EventObject {
    private boolean response;

    /**
     * Constructs an AuthReplyEvent.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public AuthReplyEvent(Object source, boolean response) {
        super(source);
        this.response = response;
    }

    public boolean getResponse() {
        return response;
    }
}

package pt.ua.es2016.ccremote.auth;

import pt.ua.es2016.ccremote.card.CardInterface;
import pt.ua.es2016.ccremote.server.ExternalApp;

/**
 * Every time an application requires for authentication, an AuthRequest Object is created.
 * This object contains all the information related to the source which requested card authentication.
 */
public class AuthRequest {
    private CardInterface citizenCard;
    private ExternalApp app;
    private String[] requestedData;

    /**
     * Constructor for an Auth Request.
     * @param app The requester application.
     * @param requestedData The requested data.
     */
    public AuthRequest(CardInterface citizenCard, ExternalApp app, String[] requestedData) throws AuthRequestException {
        if(citizenCard == null || app == null || requestedData == null)
            throw new AuthRequestException("Illegal arguments found!");

        this.citizenCard = citizenCard;
        this.app = app;
        this.requestedData = requestedData;
    }

    public String getId() {
        return app.getAppId();
    }

    public String getName() {
        return app.getAppName();
    }

    public String getMessage() {
        return app.getMsg();
    }

    public String getPublicKey() {
        return app.getPublicKey();
    }

    public CardInterface getCitizenCard() {
        return citizenCard;
    }

    /**
     * If the Auth Request is invalid, an AuthRequestException is thrown.
     */
    public class AuthRequestException extends Exception {
        public AuthRequestException(String reason) {
            super(reason);
        }
    }
}
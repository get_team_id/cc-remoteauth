package pt.ua.es2016.ccremote.auth;

/**
 * Created by davidsilva on 16/05/16.
 */
public interface AuthReplyTrigger {
    void addAuthReplyListener(AuthReplyListener listener);
    void removeAuthReplyListener(AuthReplyListener listener);
    void fireAuthReply(AuthReplyEvent event);
}

package pt.ua.es2016.ccremote;

import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notification;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import pt.ua.es2016.ccremote.auth.AuthRequest;
import pt.ua.es2016.ccremote.auth.AuthRequestEvent;
import pt.ua.es2016.ccremote.auth.AuthRequestListener;
import pt.ua.es2016.ccremote.card.*;
import pt.ua.es2016.ccremote.cartaodecidadao.CartaoDeCidadao;
import pt.ua.es2016.ccremote.gui.AuthGUIController;
import pt.ua.es2016.ccremote.gui.MainGUIController;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by davidsilva on 05/05/16.
 */
public class Main extends Application implements CardEventListener, AuthRequestListener {

    private static Class<? extends CardInterface> cardClass;
    private static Class<? extends CardReaderInterface> readerClass;

    private CardInterface card = null;

    private String location = "4.1.06";

    private static Stage window;
    private static CardEventObserver observer;
    private MainGUIController controller = null;

    private static boolean alreadyInit = false;

    public void start(Stage primaryStage) throws Exception {

        try {
            // if no custom init was performed, proceed with regular app launch.
            if (!alreadyInit) {
                cardClass = CartaoDeCidadao.class;
                readerClass = CardReader.class;
                alreadyInit = true;
            }

            window = primaryStage;
            Platform.setImplicitExit(false);

            primaryStage.setTitle("Autenticador remoto com Cartão de Cidadão");

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("main.fxml"));
            Pane grid = fxmlLoader.load();
            controller = fxmlLoader.getController();


            Scene scene = new Scene(grid);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);

            CardReaderInterface reader = readerClass.newInstance();

            if (!reader.cardReaderIsPresent()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Leitor de Cartões de Cidadão não encontrado");
                alert.setHeaderText("Não foi encontrado nenhum leitor de Cartões de Cidadão");
                alert.setContentText("Por favor, insira um leitor de Cartões de Cidadão e tente novamente.\n" +
                        "Caso o problema persista, verifique se o leitor está corretamente instalado.");

                alert.showAndWait();

                System.exit(1);
            }

            observer = new CardEventObserver();
            observer.addCardEventListener(this);
            observer.addAuthRequestListener(this);

            final CardThread ct = new CardThread(cardClass, reader, observer);
            ct.start();

            controller.init(observer, observer, observer, location);

            primaryStage.show();


            primaryStage.setOnCloseRequest(e -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Sair da aplicação");
                alert.setHeaderText("Pretende sair da aplicação?");

                alert.setContentText(
                        "Ao sair da aplicação, o serviço de autenticação remota com o Cartão de Cidadão vai ser terminado."
                );

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    try {
                        controller.stopServer();
                    } catch (Exception ex) {
                    }
                    ct.interrupt();
                    System.exit(0);
                } else {
                    e.consume();
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    public synchronized void cardEventOccurred(CardEvent evt) {
        if(evt.getEvent() != CardEvent.CARD_INSERTED) card = null;
        if(evt.getEvent() == CardEvent.CARD_INSERTED) {
            card = evt.getCard();

            System.out.println(card.getFullName());

            Platform.runLater(
                    new Runnable() {
                              @Override
                              public void run() {
                                  String title = "Cartão inserido";
                                  String message = "Foi inserido um Cartão de Cidadão.";
                                  Notification notification = Notifications.SUCCESS;

                                  TrayNotification tray = new TrayNotification();
                                  tray.setAnimation(Animations.FADE);
                                  tray.setTitle(title);
                                  tray.setMessage(message);
                                  tray.setNotification(notification);
                                  //tray.setImage(card.getPicture());
                                  //tray.showAndWait();
                                  tray.showAndDismiss(new Duration(100));
                              }
                          });

        }
        else {
            card = null;

            Platform.runLater(
                    new Runnable() {
                        @Override
                        public void run() {
                            card = null;
                            String title = "Cartão removido";
                            String message = "Foi removido um Cartão de Cidadão.";
                            Notification notification = Notifications.INFORMATION;

                            TrayNotification tray = new TrayNotification();
                            tray.setAnimation(Animations.FADE);
                            tray.setTitle(title);
                            tray.setMessage(message);
                            tray.setNotification(notification);
                            //tray.showAndWait();
                            tray.showAndDismiss(new Duration(100));
                        }
                    });
            controller.setCard(null);
        }

        System.out.println(card.getCardNumber());
        controller.setCard(card);
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void init(Class<? extends CardInterface> cardInterfaceClass, Class<? extends CardReaderInterface> cardReaderInterfaceClass) {
        // we must prevent changes at runtime.
        if(alreadyInit) return;

        // set required classes to test classes or finals.
        cardClass = cardInterfaceClass;
        readerClass = cardReaderInterfaceClass;
        alreadyInit = true;
    }

    @Override
    public void authRequestOccurred(AuthRequestEvent event) {

        Platform.runLater(new Runnable() {
            @Override public void run() {
                Stage stage = new Stage();

                stage.setTitle("Pedido de autenticação");
                stage.setResizable(false);
                stage.initOwner(Main.window);
                stage.setAlwaysOnTop(true);
                stage.centerOnScreen();
                stage.initModality(Modality.WINDOW_MODAL);

                GridPane grid = null;
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("request.fxml"));
                try {
                    grid = (GridPane) fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                AuthGUIController controller = fxmlLoader.getController();
                controller.setParams(observer, (AuthRequest) event.getSource());

                Scene scene = new Scene(grid);
                stage.setScene(scene);

                stage.show();

                stage.setOnCloseRequest(e -> controller.cancelAuth(null));
            }
        });
    }
}

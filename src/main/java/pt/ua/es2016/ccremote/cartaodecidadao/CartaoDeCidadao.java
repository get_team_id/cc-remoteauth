package pt.ua.es2016.ccremote.cartaodecidadao;

import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import pt.ua.es2016.ccremote.card.CardInterface;
import pteidlib.*;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Iterator;

public class CartaoDeCidadao implements CardInterface {

    private PTEID_ID id;
    private PTEID_PIC picture;

    public CartaoDeCidadao() throws PteidException, InterruptedException {
        System.loadLibrary("pteidlibj");

        // Minimize the impact of a fast card switch.
        // A better solution may exist.
        close();
        Thread.sleep(100);

        pteid.Init("");

        // Don't check the integrity of the ID, address and photo
        pteid.SetSODChecking(false);

        this.id = pteid.GetID();
        this.picture = getPTEIDPicture();

    }

    public PTEID_ID getID() {
        try {
            return pteid.GetID();
        } catch (PteidException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getCardNumber() {
        return getID().cardNumber;
    }

    public String getGivenName() {
        return getID().firstname;
    }

    public String getSurname() {
        return getID().name;
    }

    public String getFullName() {
        return getGivenName() + " " + getSurname();
    }

    public PTEID_ADDR getAddress() throws PteidException {
        return pteid.GetAddr();
    }

    public PTEID_Certif getCertificate() {
        return null;
    }

    public PTEID_RSAPublicKey getPublicKey() {
        return null;
    }

    public PTEID_PIC getPTEIDPicture() {
        try {
            return pteid.GetPic();
        } catch (PteidException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getCardType() {
        return 0;
    }

    @Override
    public byte[] sign(String message) {
        return null;
    }

    @Override
    public boolean verify(byte[] message) {
        return false;
    }

    @Override
    public synchronized boolean authenticate() {
        final int WRONG_PIN = -1214;
        final int CARD_REMOVED = -1105;

        try {
            pteid.VerifyPIN((byte)0x81, null);
        } catch (PteidException e) {
            switch (e.getStatus()) {
                case WRONG_PIN:
                    break;

                case CARD_REMOVED:
                    //throw new CardRemovedException();
                    close();
                    break;
            }

            return false;
        }
        return true;
    }

    public Image getPicture() {
        ByteArrayInputStream bis = new ByteArrayInputStream(this.picture.picture);

        BufferedImage bf = null;
        try {
            bf = ImageIO.read(bis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        WritableImage wr = null;
        if (bf != null) {
            wr = new WritableImage(bf.getWidth(), bf.getHeight());
            PixelWriter pw = wr.getPixelWriter();
            for (int x = 0; x < bf.getWidth(); x++) {
                for (int y = 0; y < bf.getHeight(); y++) {
                    pw.setArgb(x, y, bf.getRGB(x, y));
                }
            }
        }

        return wr;
    }

    @Override
    public synchronized void close() {
        try {
            this.id = null;
            this.picture = null;
            pteid.Exit(pteid.PTEID_EXIT_UNPOWER);

        } catch (Exception e1) {e1.printStackTrace();}
    }

    private static BufferedImage toBufferedImage(java.awt.Image src) {
        int w = src.getWidth(null);
        int h = src.getHeight(null);
        int type = BufferedImage.TYPE_INT_RGB;  // other options
        BufferedImage dest = new BufferedImage(w, h, type);
        Graphics2D g2 = dest.createGraphics();
        g2.drawImage(src, 0, 0, null);
        g2.dispose();
        return dest;
    }

}

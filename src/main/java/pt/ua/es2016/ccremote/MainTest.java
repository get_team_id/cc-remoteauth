package pt.ua.es2016.ccremote;

import pt.ua.es2016.ccremote.card.FakeCardReader;
import pt.ua.es2016.ccremote.card.FakeCard;

class MainTest {

    public static void main(String[] args) {
        Main.init(FakeCard.class, FakeCardReader.class);
        FakeCardReader.triggerCardInsertion();
        Main.main(null);
    }
}
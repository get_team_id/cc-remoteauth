package pt.ua.es2016.ccremote.card;

import javafx.scene.image.Image;
import pt.ua.es2016.ccremote.card.CardInterface;
import pteidlib.*;

public class FakeCard implements CardInterface {

    @Override
    public PTEID_ID getID() {
        return null;
    }

    @Override
    public String getCardNumber() {
        return "12345678 9 ZZ8";
    }

    @Override
    public String getGivenName() {
        return "Alberto";
    }

    @Override
    public String getSurname() {
        return "Falso";
    }

    @Override
    public String getFullName() {
        return getGivenName() + " " + getSurname();
    }

    @Override
    public PTEID_ADDR getAddress() throws PteidException {
        return null;
    }

    @Override
    public PTEID_Certif getCertificate() {
        return null;
    }

    @Override
    public PTEID_RSAPublicKey getPublicKey() {
        return null;
    }

    @Override
    public PTEID_PIC getPTEIDPicture() {
        return null;
    }

    @Override
    public Image getPicture() {
        return null;
    }

    @Override
    public int getCardType() {
        return 0;
    }

    @Override
    public byte[] sign(String message) {
        return new byte[0];
    }

    @Override
    public boolean verify(byte[] message) {
        return false;
    }

    @Override
    public boolean authenticate() {
        return false;
    }

    @Override
    public void close() {

    }
}

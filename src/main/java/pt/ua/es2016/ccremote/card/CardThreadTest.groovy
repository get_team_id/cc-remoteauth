package pt.ua.es2016.ccremote.card

import javafx.application.Platform
import pt.ua.es2016.ccremote.Main

/**
 * Created by asus on 07/06/2016.
 */
class CardThreadTest extends GroovyTestCase {

    static boolean [] testValues;
    static final boolean [] success= [true,true,true,true];
    void setUp() {
        super.setUp()

        testValues = new boolean[4];
        Main.init(FakeCard.class, FakeCardReader.class);
        //FakeCardReader.triggerCardInsertion();

    }

    void testRun() {
        new Thread() {
            @Override
            void run() {
                int i = 0;
                while(i < 4) {
                    Thread.sleep(7000);
                    System.out.println("Aqui vou eu " + FakeCardReader.cardPresent);
                    testValues[i] = (!FakeCardReader.cardPresent && i%2 != 0) || (FakeCardReader.cardPresent && i%2 == 0);
                    if(FakeCardReader.cardPresent)
                        FakeCardReader.triggerCardRemoval();
                    else FakeCardReader.triggerCardInsertion();
                    i++;
                }

                System.out.println("Cheguei");
            }
        }.start();

        Main.main(null);
        assertArrayEquals(testValues, success);
    }
}

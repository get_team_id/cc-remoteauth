package pt.ua.es2016.ccremote.card;

/**
 * Created by davidsilva on 15/05/16.
 */
public interface CardEventTrigger {
    void addCardEventListener(CardEventListener listener);
    void removeCardEventListener(CardEventListener listener);
    void fireCardEvent(CardEvent event);
}

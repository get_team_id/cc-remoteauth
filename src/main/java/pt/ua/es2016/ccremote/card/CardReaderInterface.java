package pt.ua.es2016.ccremote.card;

/**
 * An Interface every card terminal or card terminal-like object must implement.
 */
public interface CardReaderInterface {

    boolean cardReaderIsPresent();
    boolean cardWasInserted();
    boolean cardWasRemoved();
    void waitForCardEvent();

}

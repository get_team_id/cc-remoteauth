package pt.ua.es2016.ccremote.card;

import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notification;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import javafx.application.Platform;
import javafx.util.Duration;
import pteidlib.PteidException;

import javax.smartcardio.CardTerminals;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A thread that listens to changes on a Card State and triggers events based on Card State change.
 */
public class CardThread<T extends CardInterface> extends Thread {

    private static final Logger logger = Logger.getLogger(CardThread.class.getName());

    private Class<CardInterface> cardClass;
    private CardInterface insertedCard = null;
    private CardReaderInterface cardReader;
    private CardEventTrigger eventHandler;

    /**
     * Constructor for a Card Thread.
     * @param cardClass The class an inserted card must be cast to.
     * @param reader The list of card cardReader the event thread is listening to.
     * @param eventHandler The card event handler.
     */
    public CardThread(Class<CardInterface> cardClass, CardReaderInterface reader, CardEventTrigger eventHandler) {
        this.cardClass = cardClass;
        this.cardReader = reader;
        this.eventHandler = eventHandler;
    }

    public void run() {
        while (true) {
            try {

                if (cardReader.cardWasInserted()) {

                    insertedCard = cardClass.newInstance();
                    CardEvent event = new CardEvent(this, CardEvent.CARD_INSERTED, insertedCard);
                    eventHandler.fireCardEvent(event);

                    logger.log(Level.INFO, "Cartão inserido");
                }

                if (cardReader.cardWasRemoved()){
                    System.out.println("Cheguei aqui /// ");
                    insertedCard.close();
                    CardEvent event = new CardEvent(this, CardEvent.CARD_REMOVED, insertedCard);
                    eventHandler.fireCardEvent(event);
                    insertedCard = null;

                    logger.log(Level.INFO, "Cartão removido");
                }

                cardReader.waitForCardEvent();
            } catch (Exception e) {
                String message = e.getMessage();
                if(message != null) {
                    if (message.contains("-1104") || message.contains("-1105")) {
                    }

                    else {
                        //logger.log(Level.WARNING, "Erro na leitura do Cartão de Cidadão. " + e.getMessage());
                        logger.log(Level.INFO, "Remova o cartão e tente novamente. Erro: "+ e.getMessage());

                        Platform.runLater(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        String title = "Erro de leitura";
                                        String message = "Erro de leitura do cartão.";
                                        Notification notification = Notifications.ERROR;

                                        TrayNotification tray = new TrayNotification();
                                        tray.setAnimation(Animations.FADE);
                                        tray.setTitle(title);
                                        tray.setMessage(message);
                                        tray.setNotification(notification);
                                        //tray.setImage(card.getPicture());
                                        //tray.showAndWait();
                                        tray.showAndDismiss(new Duration(100));
                                    }
                                });
                    }
                }
            }


        }
    }

    @Override
    public void interrupt() {
        // close the current card session to avoid errors.
        if(insertedCard != null)
            insertedCard.close();
    }


}

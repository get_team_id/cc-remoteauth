package pt.ua.es2016.ccremote.card;

import jnasmartcardio.Smartcardio;

import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;
import javax.smartcardio.TerminalFactory;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by davidsilva on 28/05/16.
 */
public class CardReader implements CardReaderInterface {
    private CardTerminals terminals;

    public CardReader() {
        try {
            TerminalFactory factory;
            factory = TerminalFactory.getInstance("PC/SC", null, new Smartcardio());
            terminals = factory.terminals();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean cardReaderIsPresent() {
        try {
            List<CardTerminal> ctL = terminals.list();
            ctL.get(0);
        } catch (IndexOutOfBoundsException | CardException ex) {
            return false;
        }

        return true;
    }

    @Override
    public boolean cardWasInserted() {
        try {
            return terminals.list(CardTerminals.State.CARD_INSERTION).size() > 0;
        } catch (CardException e) {
            return false;
        }
    }

    @Override
    public boolean cardWasRemoved() {
        try {
            return terminals.list(CardTerminals.State.CARD_REMOVAL).size() > 0;
        } catch (CardException e) {
            return false;
        }
    }

    @Override
    public void waitForCardEvent() {
        try {
            terminals.waitForChange();
        } catch (CardException e) {
            e.printStackTrace();
        }
    }
}

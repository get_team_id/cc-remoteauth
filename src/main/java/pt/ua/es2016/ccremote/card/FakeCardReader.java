package pt.ua.es2016.ccremote.card;

/**
 * Created by asus on 06/06/2016.
 */
public class FakeCardReader implements CardReaderInterface {

    static boolean cardInserted = false;
    static boolean cardRemoved = false;

    static boolean cardPresent;
    @Override
    public synchronized boolean cardReaderIsPresent() {
        return true;
    }

    @Override
    public synchronized boolean cardWasInserted() {
        if(cardInserted) {
            cardInserted = false;
            return true;
        }
        return cardInserted;
    }

    @Override
    public synchronized boolean cardWasRemoved() {
        if(cardRemoved) {
            cardRemoved = false;
            return true;
        }
        return cardRemoved;
    }

    @Override
    public synchronized void waitForCardEvent() {
        //cardInserted = false;
        //cardRemoved = false;
    }

    public synchronized static void triggerCardInsertion() {
        cardPresent = cardInserted = true;
        cardRemoved = false;
    }

    public synchronized static void triggerCardRemoval() {
        cardRemoved = true;
        cardPresent = cardInserted = false;
    }
}
package pt.ua.es2016.ccremote.card;

import java.util.EventListener;

/**
 * An interface every class that is waiting for a Card Event to occur must implement.
 */
public interface CardEventListener extends EventListener {
    /**
     * Called every time a Card Event occurred.
     * @param evt The card Event that occurred.
     */
    void cardEventOccurred(CardEvent evt);
}

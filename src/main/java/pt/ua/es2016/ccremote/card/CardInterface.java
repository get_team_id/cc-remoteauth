package pt.ua.es2016.ccremote.card;

import javafx.scene.image.Image;
import pteidlib.*;

/**
 * An Interface every Citizen Card or Citizen Card-like object must implement.
 * Contains all methods required for reading data from the Citizen Card.
 */
public interface CardInterface {

    /**
     * Get Card ID.
     * @return Citizen Card ID.
     */
    PTEID_ID getID();

    /**
     * Get Citizen Card Number
     * @return Citizen Card Number.
     */
    String getCardNumber();

    /**
     * Get Citizen's First Name(s).
     * @return Citizen's First Name(s).
     */
    String getGivenName();

    /**
     * Get Citizen's Last Name(s).
     * @return Citizen's Last Name(s).
     */
    String getSurname();

    /**
     * Get Citizen's Full Name.
     * @return Citizen's Full Name.
     */
    String getFullName();

    /**
     * Get Citizen Address.
     * @return Citizen Address.
     */
    PTEID_ADDR getAddress() throws PteidException;

    /**
     * Get Card Certificate
     * @return Citizen Card's Certificate.
     */
    PTEID_Certif getCertificate();

    /**
     * Get Citizen Card's Public Key.
     * @return Citizen Card's Public Key.
     */
    PTEID_RSAPublicKey getPublicKey();

    /**
     * Get Citizen Picture.
     * @return Citizen Picture stored on the Citizen Card.
     */
    PTEID_PIC getPTEIDPicture();

    /**
     * Get Citizen Card Type.
     * @return Card Type.
     */
    int getCardType();

    /**
     * Sign a message to confirm card ownership.
     * @param message Message to be signed
     * @return Signed message.
     */
    byte[] sign(String message);

    /**
     * Checks if the signed message is valid.
     * @param message The signed message.
     * @return true if valid, false otherwise.
     */
    boolean verify(byte[] message);

    Image getPicture();

    boolean authenticate();

    void close();
}

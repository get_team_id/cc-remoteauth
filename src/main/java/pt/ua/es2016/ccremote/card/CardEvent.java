package pt.ua.es2016.ccremote.card;

import java.util.EventObject;

/**
 * A Card Event signalling a Card insertion or removal.
 */
public class CardEvent extends EventObject {

    public final static int CARD_REMOVED = 0;
    public final static int CARD_INSERTED = 1;

    private int event;
    private CardInterface card = null;

    /**
     * Constructor for a new Card Event.
     * @param source The event source.
     */
    public CardEvent(Object source, int event, CardInterface card) {
        super(source);

        if(event != CARD_INSERTED && event != CARD_REMOVED)
            throw new IllegalArgumentException("A card event must be set as inserted or removed.");

        this.event = event;
        this.card = card;
    }

    /**
     * Get the Card Event that occurred.
     * @return Card Event that occurred.
     */
    public int getEvent() {
        return event;
    }

    /**
     * Get Card, if any.
     * @return card.
     */
    public CardInterface getCard() { return card; }

}
package pt.ua.es2016.ccremote.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import pt.ua.es2016.ccremote.auth.*;
import pt.ua.es2016.ccremote.card.CardEvent;
import pt.ua.es2016.ccremote.card.CardEventListener;
import pt.ua.es2016.ccremote.card.CardEventTrigger;
import pt.ua.es2016.ccremote.card.CardInterface;

/**
 * This class handles the /readcard route.
 * It checks if the given data is valid and produces a response based on the card details.
 */

public class RequestHandler extends AbstractHandler implements CardEventListener, AuthReplyListener
{
    private CardInterface card;
    private AuthRequestTrigger authTrigger;
    private Boolean authenticated = null;
    private boolean running = false;
    private String location;

    private RemoteNotifier remoteNotifier = null;

    public RequestHandler(CardEventTrigger cardEventTrigger, AuthRequestTrigger authTrigger, AuthReplyTrigger replyTrigger,
                          String remoteUrl, int localPort, String location) {
        cardEventTrigger.addCardEventListener(this);
        replyTrigger.addAuthReplyListener(this);

        this.authTrigger = authTrigger;
        remoteNotifier = new RemoteNotifier(remoteUrl, localPort, location);
        this.location = location;
    }

    public void handle( String target,
                        Request baseRequest,
                        HttpServletRequest request,
                        HttpServletResponse response ) throws IOException, ServletException
    {

        response.setContentType("application/json; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);

        PrintWriter out = response.getWriter();

        if(card == null) {
            sendErrorReply(out, -2);
            baseRequest.setHandled(true);
        }

        try {
            String msg = CharStreams.toString(request.getReader());
            JSONParser parser = new JSONParser();

            JSONObject object = (JSONObject) parser.parse(msg);

            Object application = object.get("application");
            Object data = object.get("data");

            Gson gson = new Gson();

            ExternalApp app = gson.fromJson(application.toString(), ExternalApp.class);
            String[] requestedData = data.toString().split(",");

            AuthRequest request1 = null;
            request1 = new AuthRequest(card,app, requestedData);

            authenticated = null;
            authTrigger.fireAuthRequest(new AuthRequestEvent(request1));

            while(authenticated == null)
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            if(authenticated) {
                String[] numbers = card.getCardNumber().split(" ");
                String number = numbers[0]+numbers[1];
                int cardno = Integer.parseInt(number);

                String output = "{\"authenticated\": {\"cardno\": \""+cardno+"\", \"name\":\""+card.getFullName()+"\", \"location\": \""+ location + "\"}}";
                out.println(output);
            }
            else
                sendErrorReply(out, -2);




        } catch (Exception e) {
            e.printStackTrace();
        }


        baseRequest.setHandled(true);
    }

    @Override
    public void cardEventOccurred(CardEvent evt) {
        if(evt.getEvent() == CardEvent.CARD_INSERTED)
            card = evt.getCard();
        else {
            // card connection was already closed previously.
            card = null;
            authenticated = false;
        }

        if(running)
            try {
                remoteNotifier.sendEvent(evt.getEvent());
            } catch (Exception e) {
                e.printStackTrace();
            }

    }

    @Override
    public void authReplyOccurred(AuthReplyEvent event) {
        authenticated = event.getResponse();
    }

    private void sendErrorReply(PrintWriter out, int error) {
        String msg = "{\"erro\": {\"id\":" + String.valueOf(error) + ", \"msg\": \"";
        String msg2;
        switch (error) {
            case -1:
                msg2 =  "Pedido inválido.";
                break;
            case -2:
                msg2 = "A autenticação falhou.";
                break;
            default:
                msg2 = "Erro desconhecido.";
                break;
        }

        msg = msg + msg2 + "\"}}";
        out.println(msg);
    }

    public void setStatus(boolean status) throws Exception {

        // if card is inserted
        if(card != null) {
            if(status)  // if service was started
                remoteNotifier.sendEvent(CardEvent.CARD_INSERTED);
            else    // if service was stoped
                remoteNotifier.sendEvent(CardEvent.CARD_REMOVED);
        }

        this.running = status;

    }
}
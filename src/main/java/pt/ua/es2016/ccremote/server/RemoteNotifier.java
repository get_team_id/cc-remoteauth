package pt.ua.es2016.ccremote.server;

import pt.ua.es2016.ccremote.card.CardEvent;
import pt.ua.es2016.ccremote.http.HTTPRequest;

import javax.servlet.ServletException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * A class that is used to notify an external server a card event has occurred.
 */
class RemoteNotifier {
    private String remoteUrl;
    private int localPort;
    private String location;

    RemoteNotifier(String remoteUrl, int localPort, String location) {

        if(!remoteUrl.endsWith("/"))
            remoteUrl = remoteUrl+"/";

        this.remoteUrl = remoteUrl;
        this.localPort = localPort;
        this.location = location;
    }

    void sendEvent(int event) throws Exception {
        //{event: {type: inserted/removed, IP: 192.10.10.10, port: num, localização}}

        String evt = null;
        if(event == CardEvent.CARD_INSERTED)
            evt = "inserted";
        else evt = "removed";

        //URL whatismyip = new URL("http://checkip.amazonaws.com");
        //BufferedReader in = new BufferedReader(new InputStreamReader(
        //        whatismyip.openStream()));

        String ip;// = in.readLine();

        ip = "localhost";
        //System.out.println(ip);

        // {"event": {"type": "inserted", "ip": "192.168.43.102", "port": "8080"}}

        String message = "{\"event\": " +
                "{\"type\": \""+ evt + "\","
                +"\"ip\": \"" + ip + "\", " +
                "\"port\": \"" + String.valueOf(localPort) + "\"}}";

        //String msg2 = "{\"event\": {\"type\": \"inserted\", \"ip\": \"192.168.43.102\", \"port\": \"8080\"}}";
        System.out.println(message);


        HTTPRequest.sendPost(remoteUrl+"cardevent", message);
    }


}

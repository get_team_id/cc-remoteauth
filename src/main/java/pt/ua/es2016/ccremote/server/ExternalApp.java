package pt.ua.es2016.ccremote.server;

/**
 * Created by davidsilva on 29/05/16.
 */
public class ExternalApp {
    private String app_id;
    private String app_name;
    private String msg;
    private String publicKey;

    public ExternalApp(String app_id, String app_name, String msg, String publicKey) {
        this.app_id = app_id;
        this.app_name = app_name;
        this.msg = msg;
        this.publicKey = publicKey;
    }

    public String getAppId() {
        return app_id;
    }

    public String getAppName() {
        return app_name;
    }

    public String getMsg() {
        return msg;
    }

    public String getPublicKey() {
        return publicKey;
    }
}

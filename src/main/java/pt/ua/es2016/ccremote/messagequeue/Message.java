package pt.ua.es2016.ccremote.messagequeue;

/**
 * Created by davidsilva on 17/05/16.
 */
public class Message {

    private int msgID;
    private int cardID;
    private String event;
    private boolean isUAMember;

    public Message(int msgID, int cardID, String event, boolean isUAMember) {
        this.msgID = msgID;
        this.cardID = cardID;
        this.event = event;
        this.isUAMember = isUAMember;
    }

    public int getMsgID() {
        return msgID;
    }

    public int getCardID() {
        return cardID;
    }

    public String getEvent() {
        return event;
    }

    public boolean isUAMember() {
        return isUAMember;
    }
}

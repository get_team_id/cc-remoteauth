package pt.ua.es2016.ccremote.messagequeue;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import pt.ua.es2016.ccremote.card.CardEvent;
import pt.ua.es2016.ccremote.card.CardEventListener;
import pt.ua.es2016.ccremote.card.CardEventTrigger;
import pt.ua.es2016.ccremote.http.HTTPRequest;
import pt.ua.es2016.ccremote.http.HTTPResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by davidsilva on 17/05/16.
 */
public class MessageProducer implements CardEventListener {

    private Logger logger = Logger.getLogger(MessageProducer.class.getName());

    private static int msgId = 0;

    private Map<Integer, Message> messageList = new HashMap<>();

    private String server;
    private String queue;

    private static ConnectionFactory factory;
    private static Connection connection;
    private static Channel channel;

    public MessageProducer(CardEventTrigger trigger, String queue,  String server) throws IOException, TimeoutException {
        this.queue = queue;
        this.server = server;

        factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = null;
        connection = factory.newConnection();
        channel = connection.createChannel();

        /**
         * queue - the name of the queue
         * durable - true if we are declaring a durable queue (the queue will survive a server restart)
         * exclusive - true if we are declaring an exclusive queue (restricted to this connection)
         * autoDelete - true if we are declaring an autodelete queue (server will delete it when no longer in use)
         * arguments - other properties (construction arguments) for the queue
         */
        channel.queueDeclare(queue, false, false, false, null);

        trigger.addCardEventListener(this);
    }

    @Override
    public void cardEventOccurred(CardEvent evt) {
        msgId++;

        int cardno = 0;

        String event = "CARD_REMOVED";

        if(evt.getEvent() == CardEvent.CARD_INSERTED)
            event = "CARD_INSERTED";

        String[] cardnumbers;
        cardnumbers = evt.getCard().getCardNumber().split(" ");

        String tmp = cardnumbers[0]+cardnumbers[1];
        cardno = Integer.parseInt(tmp);

        boolean member = isUAMember(cardno);
        Message m = new Message(msgId, cardno, event, member);


        String messageJSON = "";

        Gson gson = new GsonBuilder().create();
        messageJSON = gson.toJson(m);

        try {
            channel.basicPublish("", queue, null, messageJSON.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.log(Level.INFO, "Sent " + messageJSON);
    }

    private boolean isUAMember(int cc) {

        if(cc == 0) return false;

        String url = "http://localhost:8080";
        HTTPResponse response = null;

        try {
            response = HTTPRequest.sendPost(url+"/checkIfUAMember",cc+"");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Boolean.getBoolean(response.getBody());
    }
}

package pt.ua.es2016.ccremote.http;

/**
 * Created by Andre on 02/04/2016.
 */
public class HTTPResponse {

    private String header;
    private String body;

    public HTTPResponse(String header, String body){
        this.header = header;
        this.body = body;
    }

    public static HTTPResponse buildResponse(String header, String body){

        HTTPResponse httpResponse = new HTTPResponse(header,body);

        return httpResponse;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

package pt.ua.es2016.ccremote.gui;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.eclipse.jetty.http.HttpCompliance;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import pt.ua.es2016.ccremote.auth.AuthReplyTrigger;
import pt.ua.es2016.ccremote.auth.AuthRequestTrigger;
import pt.ua.es2016.ccremote.card.CardEventTrigger;
import pt.ua.es2016.ccremote.card.CardInterface;
import pt.ua.es2016.ccremote.server.RequestHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by davidsilva on 05/05/16.
 */
public class MainGUIController {
    private Logger logger = Logger.getLogger(MainGUIController.class.getName());
    private RequestHandler handler = null;

    @FXML
    private Button btnStart;

    @FXML
    private Button btnStop;

    @FXML
    private TextField localPort;

    @FXML
    private TextField remoteUrl;

    @FXML
    private Text serviceStatus;

    @FXML
    private Text cardno;

    @FXML
    private Text firstname;

    @FXML
    private Text lastname;

    @FXML
    private ImageView picture;

    @FXML
    private MenuBar menuBar;

    private Server server = null;


    public void onAuthRequest() {
        // brevemente na NOS Lusomundo
    }

    public void startServer() {
        try {
            server.start();

            btnStop.setDisable(false);
            btnStart.setDisable(true);

            localPort.setDisable(true);
            remoteUrl.setDisable(true);

            serviceStatus.setText("O Serviço foi Iniciado com sucesso.");
            logger.log(Level.INFO, "O serviço foi iniciado na porta "+ localPort.getText() + ".");
            handler.setStatus(true);
        } catch (Exception e) {
            try {
                stopServer();
            } catch (Exception e1) {}


            serviceStatus.setText("O serviço está parado.");
            logger.log(Level.SEVERE, "Não foi possível iniciar o serviço.");
            logger.log(Level.SEVERE, e.toString());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ocorreu um erro");
            alert.setHeaderText("Não foi possível iniciar o serviço");
            alert.setContentText("Não foi possível estabelecer uma ligação ao servidor de autenticação remoto.");
            alert.showAndWait();

        }


    }

    public void stopServer() throws Exception {

        btnStop.setDisable(true);
        btnStart.setDisable(false);

        localPort.setDisable(false);
        remoteUrl.setDisable(false);

        serviceStatus.setText("O serviço está parado.");
        server.stop();
        handler.setStatus(false);

        logger.log(Level.INFO, "O serviço foi interrompido.");
    }

    public void init(CardEventTrigger cardEventTrigger, AuthRequestTrigger authTrigger, AuthReplyTrigger replyTrigger, String location) {
        //menuBar.setUseSystemMenuBar(true);
        int port = Integer.parseInt(localPort.getText());
        server = new Server(port);
        server.getConnectors()[0].getConnectionFactory(HttpConnectionFactory.class).setHttpCompliance(HttpCompliance.LEGACY);

        handler = new RequestHandler(cardEventTrigger, authTrigger, replyTrigger, remoteUrl.getText(), port, location);
        ContextHandler context = new ContextHandler("/");
        context.setContextPath("/readcard");
        context.setAllowNullPathInfo(true);
        context.setHandler(handler);
        server.setHandler(context);

        //setCard(null);
    }

    public void setCard(CardInterface card) {

        System.out.println("SET CARD " + card);
        if(card == null) {
            System.out.println("setCard card was null");
            picture.setImage(new Image(getClass().getClassLoader().getResource("no-card.jpg").toString()));
            firstname.setText("");
            lastname.setText("Inserir Cartão de Cidadão");
            cardno.setText("Número do Cartão");
        }
        else {
            System.out.println("setCard "+card.getFullName());
            firstname.setText(card.getGivenName());
            lastname.setText(card.getSurname());
            cardno.setText(card.getCardNumber());
            picture.setImage(card.getPicture());
        }
    }

    public void setServer(Server server) {
        this.server = server;
    }
}

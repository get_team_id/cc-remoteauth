package pt.ua.es2016.ccremote.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pt.ua.es2016.ccremote.auth.AuthReplyEvent;
import pt.ua.es2016.ccremote.auth.AuthReplyTrigger;
import pt.ua.es2016.ccremote.auth.AuthRequest;
import pt.ua.es2016.ccremote.card.CardInterface;

import javax.smartcardio.Card;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by davidsilva on 16/05/16.
 */
public class AuthGUIController {

    private AuthReplyTrigger trigger;
    private CardInterface card;
    private Logger logger = Logger.getLogger(AuthGUIController.class.getName());

    @FXML
    private Text basic_appname;

    @FXML
    private TextArea basic_message;

    @FXML
    private Text advanced_appid;

    @FXML
    private Text advanced_appname;

    @FXML
    private TextArea advanced_pubkey;


    private AuthRequest request;

    public void setParams(AuthReplyTrigger trigger, AuthRequest request) {
        this.request = request;
        basic_appname.setText(request.getName());
        basic_message.setText(request.getMessage());
        advanced_appid.setText(request.getId());
        advanced_appname.setText(request.getName());
        advanced_pubkey.setText(request.getPublicKey());

        this.trigger = trigger;
        this.card = request.getCitizenCard();

        logger.log(Level.INFO, "Pedida autenticação pela aplicação "+ request.getId() + ".");
    }

    public void allowAuth(ActionEvent event) {
        boolean pincorrect = false;
        //try {
            pincorrect = card.authenticate();
        /*} catch (CardRemovedException e) {
            e.printStackTrace();
            System.out.println("UIUIUI");
        }*/
        trigger.fireAuthReply(new AuthReplyEvent(this, pincorrect));
        Stage stage = (Stage) basic_appname.getScene().getWindow();
        stage.close();

        if(pincorrect)
            logger.log(Level.INFO, "Concedida autenticação à aplicação "+ request.getId() + ".");
        else
            logger.log(Level.SEVERE, "Código PIN incorreto ou cartão removido.");
    }

    public void cancelAuth(ActionEvent event) {
        trigger.fireAuthReply(new AuthReplyEvent(this, false));
        Stage stage = (Stage) basic_appname.getScene().getWindow();
        stage.close();

        logger.log(Level.INFO, "Negada autenticação à aplicação "+ request.getId() + ".");
    }
}

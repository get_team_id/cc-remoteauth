package pt.ua.es2016.ccremote;

import pt.ua.es2016.ccremote.auth.*;
import pt.ua.es2016.ccremote.card.CardEvent;
import pt.ua.es2016.ccremote.card.CardEventListener;
import pt.ua.es2016.ccremote.card.CardEventTrigger;

import javax.swing.event.EventListenerList;

/**
 * CardListener is a listener and handler for card events occurring in the system.
 * It contains methods to add and remove event cardEventlisteners, and global variables
 */
public class CardEventObserver implements CardEventTrigger, AuthRequestTrigger, AuthReplyTrigger {

    private EventListenerList cardEventlisteners = new EventListenerList();
    private EventListenerList authRequestEventlisteners = new EventListenerList();
    private EventListenerList authReplyEventlisteners = new EventListenerList();

    public synchronized void addCardEventListener(CardEventListener listener) {
        cardEventlisteners.add(CardEventListener.class, listener);
    }

    public synchronized void removeCardEventListener(CardEventListener listener) {
        cardEventlisteners.remove(CardEventListener.class, listener);
    }

    public synchronized void fireCardEvent(CardEvent event) {
        for (CardEventListener listener : cardEventlisteners.getListeners(CardEventListener.class))
            listener.cardEventOccurred(event);
    }

    @Override
    public void addAuthRequestListener(AuthRequestListener listener) {
        authRequestEventlisteners.add(AuthRequestListener.class, listener);
    }

    @Override
    public void removeAuthRequestListener(AuthRequestListener listener) {
        authRequestEventlisteners.remove(AuthRequestListener.class, listener);
    }

    @Override
    public void fireAuthRequest(AuthRequestEvent event) {
        for (AuthRequestListener listener : authRequestEventlisteners.getListeners(AuthRequestListener.class))
            listener.authRequestOccurred(event);
    }

    @Override
    public void addAuthReplyListener(AuthReplyListener listener) {
        authReplyEventlisteners.add(AuthReplyListener.class, listener);
    }

    @Override
    public void removeAuthReplyListener(AuthReplyListener listener) {
        authReplyEventlisteners.add(AuthReplyListener.class, listener);
    }

    @Override
    public void fireAuthReply(AuthReplyEvent event) {
        for (AuthReplyListener listener : authReplyEventlisteners.getListeners(AuthReplyListener.class))
            listener.authReplyOccurred(event);
    }
}
